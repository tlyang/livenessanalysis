This program works in identifying the variables that are live coming into a statement.

Some stuff that is not working:
	- Properly identifying out variables for the last line
		of code (before the line showing which variable is live out of the
		code).
		
Testing strategy: Regression
- Run the inputs of the example code in the MOOC,
	then solve it by hand, and compare.
	
	
	
The output.txt contains the output for the following code:

X := 2
Label1:
Y := X + 1
if Z > 8 goto Label2
X := 3
X := X + 5
Y := X + 5
X := 2
if Z > 10 goto Label1
X := 3
Label2:
Y := X + 2
X := 0
goto Label3
X := 10
X := X + X
Label3:
Y := X + 1
# Y