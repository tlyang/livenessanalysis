import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

/**
 * Performs liveness analysis on a "three address code" program
 * 	as shown in the Stanford Compilers MOOC
 * 
 * @author Thomas Yang (0121)
 *
 */
public class live {
	private static final String SP = " ";
	private static final String HT = "#";
	private static final String LABEL = "Label";
	private static final String GOTO = "goto";
	private static final String IF = "if";
	private static final String VAR_REGEX = "[a-zA-Z]";
	
	private Stack<Code> stack1;
	private Scanner in;
	private Map<String, SortedSet<String>> labelMap;
	
	public live(){
		stack1 = new Stack<Code>();
		labelMap = new HashMap<String, SortedSet<String>>();
		
		analyzeForLiveness();
	}
	
	private void analyzeForLiveness(){
		in = new Scanner(System.in); 
		
		String input;
		
		while(in.hasNextLine()){
			input = in.nextLine();
			stack1.push(new Code(input));
		}
		
		propagatePass();
		labelPass();
		propagatePass();
		print(stack1);
	}
	
	private void propagatePass(){
		Code code;
		String [] s;
		int i;
		Stack<Code> s_t = new Stack<Code>();

		// the propagate list keeps track of the live variables
		//	when traversing backwards through the stack
		SortedSet<String> propagate = new TreeSet<String>();
		
		// Find the live variable at the end from the input
		if(stack1.peek().getCodeLine().trim().lastIndexOf(HT) == 0){
			code = stack1.pop();
			s = code.getCodeLine().split(SP);
			
			for(i=1; i<s.length; i++){
				propagate.add(s[i]);
			}
			
			code.getLiveIn().addAll(propagate);
			s_t.push(code);
		}
		
		while(!stack1.empty()){
			code = stack1.pop();
			s = code.getCodeLine().split(SP);

			// Add all the live variables coming in into the
			//  temporary list.
			propagate.addAll(code.getLiveIn());
			code.getLiveOut().addAll(propagate);
			
			if(code.getCodeLine().trim().indexOf(LABEL) == 0){	
				/*
				 * Reach a line declaring a Label
				 * 	Save the live variables at this point
				 */
				labelMap.put(returnLabel(code.getCodeLine()), new TreeSet<String>(propagate));
			}else{
				/*
				 * Reach a line of code
				 */
				for(i=0; i<s.length; i++){
					String obj = s[i];
					
					if(isVar(obj)){
						if(i == 0){
							// If a variable is the first token, 
							//	then it must be used as an assignment
							propagate.remove(obj);
						}else{
							propagate.add(obj);
						}
					}
				}
			}
			code.getLiveIn().addAll(propagate);
			s_t.push(code);
		}
		stack1 = reverseStack(s_t);
	}
	
	/**
	 * Ensure that the live variables from each Label is propagated to 
	 * 	the line that declared a branch to it.
	 */
	private void labelPass(){
		Code code;
		Stack<Code> s_t = new Stack<Code>();
		SortedSet<String> propagate = new TreeSet<String>();
		int i;
		String [] s;
		
		while(!stack1.isEmpty()){
			code = stack1.pop();
			
			if(code.getCodeLine().contains(GOTO)){
				String label = returnLabel(code.getCodeLine());
				for(String v : labelMap.get(label)){
					code.getLiveIn().add(v);
				}
				propagate.addAll(code.getLiveIn());
			}
			s_t.push(code);
		}
		stack1 = reverseStack(s_t);
	}

	private void print(Stack<Code> stack){
		for(Code c : stack1){
			if(!c.getCodeLine().startsWith(HT))
				System.out.println(retAliveAsString(c.getLiveIn()));
			System.out.println(c.getCodeLine());
			if(!c.getCodeLine().startsWith(HT))
				System.out.println(retAliveAsString(c.getLiveOut()));
		}
	}
	
	private String returnLabel(String line){
		String t_line = line.trim();
		if(t_line.endsWith(":")){
			return t_line.substring(t_line.indexOf(LABEL), t_line.length()-1);
		}else if(t_line.contains(GOTO)){
			return t_line.substring(t_line.indexOf(LABEL), t_line.length());
		}
		return t_line;
	}
	
	/**
	 * Reverses the order of a stack.
	 * 
	 * @param Stack<T>
	 * @return the input stack in reverse order
	 */
	private <T> Stack<T> reverseStack(Stack<T> stack){
		Stack<T> ts = new Stack<T>();
		
		while(!stack.isEmpty()){
			ts.push(stack.pop());
		}
		return ts;
	}
	
	/**
	 * Makes a string of all the items in the input collection
	 * 
	 * @param Collection<T>
	 * @return String
	 */
	private <T> String retAliveAsString(Collection <T> coll){
		String out = HT;
		for(T t : coll){
			out += " " + t;
		}
		return out;
	}
	
	private boolean isVar(String s){
		return (s.length() == 1 && s.matches(VAR_REGEX));
	}
	
	/**
	 *	Inner class to hold each line and its line number; 
	 */
	class Code{
		private String codeLine;
		private SortedSet<String>liveIn;
		private SortedSet<String>liveOut;
		
		public Code(String codeLine){
			this.codeLine = codeLine;
		}

		public SortedSet<String> getLiveIn(){
			if(liveIn == null){
				liveIn = new TreeSet<String>();
			}
			return liveIn;
		}
		
		public SortedSet<String> getLiveOut(){
			if(liveOut == null){
				liveOut = new TreeSet<String>();
			}
			return liveOut;
		}

		public String getCodeLine() {
			return codeLine;
		}

		public void setCodeLine(String codeLine) {
			this.codeLine = codeLine;
		}
	}
	
	public static void main(String [] args){
		new live();
	}
}